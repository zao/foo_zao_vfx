solution "foo_zao_vfx"
  configurations {
    "Debug",
    "Release",
  }
  
  platforms {
    "x32",
  }

  language "C++"
  startproject "foo_zao_vfx"

  defines {
    "_WIN32_WINNT=0x0601",
    "_CRT_SECURE_NO_DEPRECATE",
    "_CRT_SECURE_NO_WARNINGS",
    "_SCL_SECURE_NO_DEPRECATE",
    "_SCL_SECURE_NO_WARNINGS",
    "UNICODE", "_UNICODE",
  }
  local sdk_dir = "./"
  local vk_dir = "C:/VulkanSDK/1.0.37.0/"

  buildoptions { "/wd4005", "/wd4996" }

  includedirs {
    ".",
    sdk_dir,
    sdk_dir .. "foobar2000",
    sdk_dir .. "wtl",
  }
  
  libdirs {
    sdk_dir .. "foobar2000/shared",
  }

  linkoptions {
    "/DEBUG:FULL",
  }

  flags {
    -- "ExtraWarnings",
    "MinimumWarnings",
    "NoEditAndContinue",
    "NoImportLib",
    "NoIncrementalLink",
    "StaticRuntime",
    "Symbols",
  }

  location (_ACTION)
  targetdir (_ACTION .. "/bin")
  objdir (_ACTION .. "/obj")

project "pfc"
  kind "StaticLib"
  uuid "635AC26A-7CC9-45A2-A3ED-8602AB25101E"

  excludes {
    sdk_dir .. "pfc/stdafx.cpp",
    sdk_dir .. "pfc/selftest.cpp",
    sdk_dir .. "pfc/synchro_nix.cpp",
    sdk_dir .. "pfc/nix-objects.cpp",
  }

  files {
    sdk_dir .. "pfc/*.cpp",
    sdk_dir .. "pfc/*.h",
  }

project "component"
  kind "StaticLib"
  uuid "952ACFF4-8CD3-490A-957E-501FED91D9C6"

  excludes {
    sdk_dir .. "foobar2000/ATLHelpers/stdafx.*",
    sdk_dir .. "foobar2000/helpers/StdAfx.*",
    sdk_dir .. "foobar2000/SDK/stdafx.*",
  }

  files {
    sdk_dir .. "foobar2000/ATLHelpers/**.cpp",
    sdk_dir .. "foobar2000/ATLHelpers/**.h",
    sdk_dir .. "foobar2000/foobar2000_component_client/**.cpp",
    sdk_dir .. "foobar2000/helpers/**.cpp",
    sdk_dir .. "foobar2000/helpers/**.h",
    sdk_dir .. "foobar2000/SDK/**.cpp",
    sdk_dir .. "foobar2000/SDK/**.h",
    sdk_dir .. "foobar2000/shared/**.h",
  }

project "columns_ui_sdk"
  kind "StaticLib"
  uuid "F709245B-3839-4E0C-9E45-1EEF243E998A"

  excludes {
    sdk_dir .. "**/stdafx.cpp",
  }

  files {
    sdk_dir .. "foobar2000/columns_ui-sdk/**.cpp",
    sdk_dir .. "foobar2000/columns_ui-sdk/**.h",
  }

project "foo_zao_vfx"
  kind "SharedLib"
  uuid "3E10CEF6-4AFC-4A9F-B96A-5D91931B57E6"

  targetdir "portable/user-components/foo_zao_vfx"

  defines { "VK_USE_PLATFORM_WIN32_KHR" }

  includedirs {
    path.join(vk_dir, "Include")
  }

  libdirs {
    path.join(vk_dir, "Bin32")
  }
  local run_dir = path.join(solution().basedir, "portable/user-components/foo_zao_vfx/")

  postbuildcommands {
    -- "copy " .. path.translate(glew_dll) .. " " .. path.translate(run_dir),
    -- "copy " .. path.translate(glfw_dll) .. " " .. path.translate(run_dir)
  }

  links {
    "component",
    "pfc",
    "shared",
    "columns_ui_sdk",
    "rpcrt4",
    "vulkan-1",
    "ws2_32", "userenv", "shell32", "advapi32", "winmm",
  }

  files {
    "foo_zao_vfx/**.c",
    "foo_zao_vfx/**.cc",
    "foo_zao_vfx/**.h",
  }

  configuration "Debug"
    links {}

  configuration "Release"
    links {}