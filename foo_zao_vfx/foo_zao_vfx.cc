﻿#include <ATLHelpers/ATLHelpers.h>
#include <atomic>
#include <mutex>
#include <queue>
#include <set>

#include <vulkan/vulkan.hpp>

#include <atlbase.h>
#include <atlapp.h>
#include <atlframe.h>
#include <atlcrack.h>

using i8 = int8_t;
using i16 = int16_t;
using i32 = int32_t;
using i64 = int64_t;
using u8 = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;
using f32 = float;
using f64 = double;
using b8 = bool;
using b32 = uint32_t;

template <typename M>
struct message_queue {
  void push(M&& m)
  {
    lock_t lk(_mtx);
    _q.push(std::move(m));
  }

  void get_all(std::vector<M>& sink) {
    while (!_q.empty()) {
      sink.push_back(_q.front());
      _q.pop();
    }
  }

  using lock_t = std::lock_guard<std::mutex>;
  std::mutex _mtx;
  std::queue<M> _q;
};

struct fx_free_window : WTL::CFrameWindowImpl<fx_free_window> {
  DECLARE_WND_CLASS_EX(L"foo_zao_vfx_free_window", CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS, 0)

protected:
  virtual void fx_create() = 0;
  virtual void fx_close() = 0;

public:
  virtual void fx_tick() = 0;

private:
  BEGIN_MSG_MAP(fx_free_window)
    MSG_WM_CREATE(on_wm_create)
    MSG_WM_DESTROY(on_wm_destroy)
    MSG_WM_PAINT(on_wm_paint)
    MSG_WM_ERASEBKGND(on_wm_erasebkgnd)
  END_MSG_MAP()

  LRESULT on_wm_create(CREATESTRUCT* cs) {
    timeBeginPeriod(1);
    fx_create();
    return 0;
  }

  void on_wm_destroy() {
    fx_close();
    timeEndPeriod(1);
  }

  void on_wm_paint(HDC dc) { ValidateRgn(nullptr); }

  LRESULT on_wm_erasebkgnd(CDCHandle dc) { return TRUE; }
};

static wchar_t const* g_moo_class_name = L"{5FC5D0BD-F88B-467E-BE4F-D7E0D0BE5706}";

struct moo_fx_free : fx_free_window {
protected:
  void fx_create() override;
  void fx_close() override;

public:
  void fx_tick() override;

private:
  vk::Instance _instance;
  vk::Device _dev;
  vk::SurfaceKHR _surf;
};

// {C0C3A902-32AB-4410-9D49-2AC58747C89F}
static GUID const moo_fx_guid = { 0xc0c3a902, 0x32ab, 0x4410,{ 0x9d, 0x49, 0x2a, 0xc5, 0x87, 0x47, 0xc8, 0x9f } };

void
moo_fx_free::fx_create() {
  vk::ApplicationInfo ai{
    "foo_zao_vfx",
    VK_MAKE_VERSION(1, 0, 0)
  };
  std::vector<char const*> instance_exts;
  instance_exts.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
  vk::InstanceCreateInfo ci{
    vk::InstanceCreateFlags{},
    &ai, 0, nullptr, instance_exts.size(), instance_exts.data()
  };
  _instance = vk::createInstance(ci, nullptr);
  auto physical_devices = _instance.enumeratePhysicalDevices();
  console::print("[vk] Enumerating physical devices...");
  for (auto& pdev : physical_devices) {
    auto props = pdev.getProperties();
    console::printf("[vk] %s", props.deviceName);
  }
  console::print("[vk] Enumeration complete.");
  if (physical_devices.empty()) {
    return;
  }
  auto chosen_pdev = std::move(physical_devices.front());
  auto queue_families = chosen_pdev.getQueueFamilyProperties();
  u32 chosen_queue = 0;
  for (; chosen_queue < queue_families.size(); ++chosen_queue) {
    auto i = chosen_queue;
    auto& qf = queue_families[i];
    b32 supports_presentation = chosen_pdev.getWin32PresentationSupportKHR(chosen_queue);
    if (supports_presentation && qf.queueFlags & (vk::QueueFlagBits::eGraphics | vk::QueueFlagBits::eCompute | vk::QueueFlagBits::eTransfer)) {
      break;
    }
  }
  if (chosen_queue == queue_families.size()) {
    return;
  }
  vk::DeviceQueueCreateInfo qci{ {}, chosen_queue, 1 };
  std::vector<char const*> device_extensions;
  device_extensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
  _dev = chosen_pdev.createDevice(vk::DeviceCreateInfo{ {}, 1, &qci, 0, nullptr, device_extensions.size(), device_extensions.data() });
  HWND wnd = *this;
  _surf = _instance.createWin32SurfaceKHR(vk::Win32SurfaceCreateInfoKHR{ {}, nullptr, wnd });
  
}

void
moo_fx_free::fx_close() {
  if (_surf) {
    _instance.destroySurfaceKHR(_surf);
  }
  if (_dev) {
    _dev.destroy();
  }
  if (_instance) {
    _instance.destroy();
  }
}

void
moo_fx_free::fx_tick() {
  char buf[100]{};
  i64 pc = _Query_perf_counter();
  double pf = (double)_Query_perf_frequency();
  double t = pc / pf;
  snprintf(buf, _countof(buf), "[tick] Boop @ %f\n", t);
  OutputDebugStringA(buf);
  Sleep(1000 / 144);
}

struct fx_thread : std::enable_shared_from_this<fx_thread> {
  void start(fx_free_window* wnd);
  void stop();

private:
  void proc(fx_free_window* wnd);
  std::thread _thread;
  message_queue<i32> _in_queue;
};

static std::vector<std::shared_ptr<fx_thread>> g_fx_threads;

void fx_thread::start(fx_free_window* wnd) {
  auto self = shared_from_this();
  g_fx_threads.push_back(self);
  _thread = std::thread(&fx_thread::proc, self, wnd);
}

void fx_thread::stop() {
  _in_queue.push(0);
}

void
fx_thread::proc(fx_free_window* wnd) {
  wnd->Create(nullptr, nullptr, L"Moo", WS_OVERLAPPEDWINDOW, WS_EX_OVERLAPPEDWINDOW, nullptr, nullptr);
  wnd->ShowWindow(SW_NORMAL);
  while (1) {
    MSG msg{};
    while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }
    std::vector<i32> in_msgs;
    _in_queue.get_all(in_msgs);
    if (!in_msgs.empty()) {
      break;
    }

    wnd->fx_tick();
  }
  if (*wnd) {
    wnd->DestroyWindow();
  }
}

struct vis_menu : mainmenu_commands
{
  vis_menu() {}

  u32 get_command_count() override { return 1; }

  GUID get_command(u32 idx) override { return moo_fx_guid; }

  void get_name(u32 idx, pfc::string_base& out) override
  {
    out = "Moo";
  }

  bool get_description(u32 idx, pfc::string_base& out) override
  {
    return false;
  }

  GUID get_parent() override { return mainmenu_groups::view_visualisations; }

  void execute(u32 idx, service_ptr_t<service_base>) override
  {
    std::shared_ptr<fx_thread> fxt = std::make_shared<fx_thread>();
    auto* wnd = new moo_fx_free();
    fxt->start(wnd);
  }
};

static mainmenu_commands_factory_t<vis_menu> g_vis_menu;

struct iq : initquit {
  void on_quit() override {
    for (auto& fxt : g_fx_threads) {
      fxt->stop();
      fxt.reset();
    }
  }
};

static initquit_factory_t<iq> g_iq;

DECLARE_COMPONENT_VERSION("VFX", "1.0.0", "zao")
VALIDATE_COMPONENT_FILENAME("foo_zao_vfx.dll")
